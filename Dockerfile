#builder section
FROM node:20.3.0-alpine3.17 as builder
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

#production section
FROM node:20.3.0-alpine3.17
WORKDIR /app
# COPY --from=builder /app/package*.json ./
# RUN npm install --only=production
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/node_modules ./node_modules
EXPOSE 3000
CMD [ "node", "dist/main.js" ]
