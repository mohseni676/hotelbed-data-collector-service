import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HotelbedsModule } from './hotelbeds/hotelbeds.module';
import { BullModule, BullModuleOptions } from '@nestjs/bull';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const uri = configService.get<string>('MONGO_URI');
        console.log(uri);
        return { uri };
      },
      inject: [ConfigService],
      imports: [ConfigModule],
    }),
    BullModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const confing:BullModuleOptions = {
          redis: {
            host: configService.get<string>('REDIS_HOST'),
            port: configService.get<number>('REDIS_PORT'),
          }
        }
        return confing;
      },
      inject: [ConfigService],
      imports: [ConfigModule],
    }),
    HotelbedsModule
  ],
})
export class AppModule {}
