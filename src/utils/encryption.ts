import * as CryptoJs from 'crypto-js';
export function generateXSignature(secretKey: string, apiKey: string) {
    const crypto = require('crypto');
    const timestamp = Math.floor(new Date().getTime() / 1000);
    const signature = `${apiKey}${secretKey}${timestamp}`;
    const xSignatureOld = CryptoJs.SHA256(signature).toString(CryptoJs.enc.Hex);
    const xSignature = crypto.createHash('sha256').update(signature).digest('hex');
    return { timestamp, xSignature };
}