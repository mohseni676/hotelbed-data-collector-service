import { Controller, Get } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { DataCollectorService } from "../services/data-collector.service";

@ApiTags('hotel')
@Controller('hotel')
export class HotelController {

    constructor(
        private readonly dataCollectorService: DataCollectorService
    ) {}


    @Get('/collect')
    async collect() {
        return await this.dataCollectorService.createDataCollectorChunks();
    }

    @Get('/total')
    async getTotal() {
        return await this.dataCollectorService.getTotalHotels();
    }

    @Get('/api-status')
    async getApiStatus() {
        return await this.dataCollectorService.getStatus();
    }
}