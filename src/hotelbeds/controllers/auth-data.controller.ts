import { Body, Controller, Get, Post } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthDataService } from "../services/auth-data.service";
import { createAuthDataDto } from "../dto/create-auth-data.dto";

@ApiTags('auth-data')
@Controller('auth-data')
export class AuthDataController {
    constructor(
        private readonly authDataService: AuthDataService
    ) {}

    @Post()
    async createAuthData(@Body() createAuthDataDto: createAuthDataDto) {
        return await this.authDataService.createAuthData(createAuthDataDto);
    }

    @Get('all')
    async getAuthData() {
        return await this.authDataService.getAuthData();
    }
}