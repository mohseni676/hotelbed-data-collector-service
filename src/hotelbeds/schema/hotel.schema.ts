import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { City, CountryCode, Coordinates, CategoryGroupCode, AccommodationTypeCode, Address, Phone, Room, Facility, Terminal, InterestPoint, Wildcard, S2C, Image } from "../interfaces/content.interface";
import { Mongoose } from "mongoose";

@Schema({
    collection: 'hotels',
    timestamps: true
})
export class Hotel {
    @Prop()
    code: number;
    @Prop()
    name: City;
    @Prop()
    description: City;
    @Prop({type: String})
    countryCode: string;
    @Prop()
    stateCode: string;
    @Prop()
    destinationCode: string;
    @Prop()
    zoneCode: number;
    @Prop()
    coordinates: Coordinates;
    @Prop()
    categoryCode: string;
    @Prop({type: String,})
    categoryGroupCode: string;
    @Prop()
    chainCode: string;
    @Prop({type: String, enum: AccommodationTypeCode})
    accommodationTypeCode: AccommodationTypeCode;
    @Prop({type: [String]})
    boardCodes: string[];
    @Prop({type: [Number]})
    segmentCodes: number[];
    @Prop()
    address: Address;
    @Prop()
    postalCode: string;
    @Prop()
    city: City;
    @Prop()
    email?: string;
    @Prop()
    license?: string;
    @Prop()
    phones: Phone[];
    @Prop()
    rooms: Room[];
    @Prop()
    facilities: Facility[];
    @Prop()
    terminals?: Terminal[];
    @Prop()
    interestPoints?: InterestPoint[];
    @Prop()
    images: Image[];
    @Prop()
    wildcards?: Wildcard[];
    @Prop()
    web?: string;
    @Prop()
    lastUpdate: Date;
    @Prop()
    S2C: S2C;
    @Prop()
    ranking: number;
}

export const HotelSchema = SchemaFactory.createForClass(Hotel)