import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

export type AuthDataDocument = AuthData & Document;
@Schema({
    timestamps: true
})
export class AuthData {
    @Prop()
    apiKey: string;
    @Prop()
    secretKey: string;
    @Prop({default: false})
    expired: boolean;
}

export const AuthDataSchema = SchemaFactory.createForClass(AuthData)