import { Module } from '@nestjs/common';
import { DataCollectorService } from './services/data-collector.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Hotel, HotelSchema } from './schema/hotel.schema';
import { AuthDataSchema } from './schema/auth-data.schema';
import { AuthDataService } from './services/auth-data.service';
import { HotelController } from './controllers/hotel.controller';
import { AuthDataController } from './controllers/auth-data.controller';
import { BullModule } from '@nestjs/bull';
import { DataCollectorConsumer } from './consumers/data-collector.consumer';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Hotel', schema: HotelSchema },
            { name: 'AuthData', schema: AuthDataSchema }
        ]),
        BullModule.registerQueue({
            name: 'data-collector',
            // settings: {
            //     stalledInterval: 10000
            // }
        })
    ],
    providers: [
        DataCollectorService,
        AuthDataService,
        DataCollectorConsumer
    ],
    controllers: [
        HotelController,
        AuthDataController
    ],
})
export class HotelbedsModule {}
