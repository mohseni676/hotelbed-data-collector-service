export class HotelsDto {
    from: number;
    to: number;
    total: number;
    auditData: AuditData;
    hotels: HotelDto[];
}

export class AuditData {
    processTime: string;
    timestamp: Date;
    requestHost: string;
    serverId: string;
    environment: string;
    release: string;
}

export class HotelDto {
    code: number;
    name: City;
    description: City;
    countryCode: CountryCode;
    stateCode: string;
    destinationCode: string;
    zoneCode: number;
    coordinates: Coordinates;
    categoryCode: string;
    categoryGroupCode: CategoryGroupCode;
    chainCode?: string;
    accommodationTypeCode: AccommodationTypeCode;
    boardCodes: string[];
    segmentCodes: number[];
    address: Address;
    postalCode: string;
    city: City;
    email?: string;
    license?: string;
    phones: Phone[];
    rooms: Room[];
    facilities: Facility[];
    terminals?: Terminal[];
    interestPoints?: InterestPoint[];
    images: Image[];
    wildcards?: Wildcard[];
    web?: string;
    lastUpdate: Date;
    S2C: S2C;
    ranking: number;
}

export enum S2C {
    The1 = "1*",
    The2 = "2*",
    The3 = "3*",
    The4 = "4*",
    The5 = "5*",
    The6 = "6*",
    The7 = "7*",
}

export enum AccommodationTypeCode {
    A = "A",
    B = "B",
    C = "C",
    D = "D",
    E = "E",
    F = "F",
    G = "G",
    H = "H",
    I = "I",
    J = "J",
    K = "K",
    L = "L",
    M = "M",
    N = "N",
    O = "O",
    P = "P",
    Q = "Q",
    R = "R",
    S = "S",
    T = "T",
    U = "U",
    V = "V",
    W = "W",
    X = "X",
    Y = "Y",
    Z = "Z",

}

export class Address {
    content: string;
    street: string;
    number: string;
    floor?: string;
}

export enum CategoryGroupCode {
    Grupo1 = "GRUPO1",
    Grupo2 = "GRUPO2",
    Grupo3 = "GRUPO3",
    Grupo4 = "GRUPO4",
    Grupo5 = "GRUPO5",
    Grupo6 = "GRUPO6",
    Grupo7 = "GRUPO7",
    Grupo8 = "GRUPO8",
    Grupo9 = "GRUPO9",
    Grupo10 = "GRUPO10",
    Grupo11 = "GRUPO11",
    Grupo12 = "GRUPO12",
    Grupo13 = "GRUPO13",
    Grupo14 = "GRUPO14",
    Grupo15 = "GRUPO15",
}

export class City {
    content: string;
}

export class Coordinates {
    longitude: number;
    latitude: number;
}

export enum CountryCode {
    Es = "ES",
}

export class Facility {
    facilityCode: number;
    facilityGroupCode: number;
    order: number;
    indYesOrNo?: boolean;
    number?: number;
    voucher: boolean;
    indLogic?: boolean;
    indFee?: boolean;
    distance?: number;
    timeFrom?: string;
    timeTo?: string;
    dateTo?: Date;
    amount?: number;
    currency?: string;
    applicationType?: string;
    ageFrom?: number;
}

export class Image {
    imageTypeCode: ImageTypeCode;
    path: string;
    order: number;
    visualOrder: number;
    roomCode?: string;
    roomType?: RoomTypeEnum;
    characteristicCode?: string;
}

export enum ImageTypeCode {
    Bar = "BAR",
    COM = "COM",
    Con = "CON",
    Dep = "DEP",
    Gen = "GEN",
    Hab = "HAB",
    Pis = "PIS",
    Pla = "PLA",
    Res = "RES",
    Ter = "TER",
}

export enum RoomTypeEnum {
    Apt = "APT",
    Bed = "BED",
    Ctg = "CTG",
    Dbl = "DBL",
    Dbt = "DBT",
    Dus = "DUS",
    Fam = "FAM",
    Jsu = "JSU",
    Qua = "QUA",
    Roo = "ROO",
    Sgl = "SGL",
    Sui = "SUI",
    Tpl = "TPL",
    Twn = "TWN",
}

export class InterestPoint {
    facilityCode: number;
    facilityGroupCode: number;
    order: number;
    poiName: string;
    distance: string;
}

export class Phone {
    phoneNumber: string;
    phoneType: PhoneType;
}

export enum PhoneType {
    Faxnumber = "FAXNUMBER",
    Phonebooking = "PHONEBOOKING",
    Phonehotel = "PHONEHOTEL",
    Phonemanagement = "PHONEMANAGEMENT",
}

export class Room {
    roomCode: string;
    isParentRoom: boolean;
    minPax: number;
    maxPax: number;
    maxAdults: number;
    maxChildren: number;
    minAdults: number;
    roomType: RoomTypeEnum;
    characteristicCode: string;
    roomFacilities?: RoomFacility[];
    roomStays?: RoomStay[];
    PMSRoomCode?: string;
}

export class RoomFacility {
    facilityCode: number;
    facilityGroupCode: number;
    indLogic?: boolean;
    number?: number;
    voucher: boolean;
    indYesOrNo?: boolean;
    indFee?: boolean;
}

export class RoomStay {
    stayType: StayType;
    order: string;
    description?: Description;
    roomStayFacilities?: RoomStayFacility[];
}

export enum Description {
    BedRoom = "Bed room",
}

export class RoomStayFacility {
    facilityCode: number;
    facilityGroupCode: number;
    number: number;
}

export enum StayType {
    Bed = "BED",
    Liv = "LIV",
}

export class Terminal {
    terminalCode: string;
    distance: number;
}

export class Wildcard {
    roomType: string;
    roomCode: RoomTypeEnum;
    characteristicCode: string;
    hotelRoomDescription: City;
}
