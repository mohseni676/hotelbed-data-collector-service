import { Process, Processor } from "@nestjs/bull";
import { DataCollectorService } from "../services/data-collector.service";
import { AuthDataService } from "../services/auth-data.service";
import { QueueDto } from "../dto/queue.dto";

@Processor('data-collector')
export class DataCollectorConsumer{
    constructor(
        private readonly dataCollectorService: DataCollectorService,
        private readonly authDataService: AuthDataService
    ) {}

    @Process()
    async handleDataCollector(job: any) {
        const authData = await this.authDataService.getNewAuthData();
        if(!authData) {
            //stop job process
            console.log('Process error',job.data,'AuthData not found');
            await job.moveToFailed(new Error('AuthData not found'), 0);
        }
        try{
            const response = await this.dataCollectorService.getHotels(job.data.from, job.data.to, authData.apiKey, authData.secretKey);
            if(response.status === 'success') {
                await job.moveToCompleted();
            }
        }catch(err) {
            //stop job process
            console.log('Process error',job.data,err.message);
            await job.moveToFailed(err, 0);
        }
    }
}