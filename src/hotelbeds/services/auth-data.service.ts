import { ConflictException, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import mongoose, { Model } from "mongoose";
import { AuthData } from "../schema/auth-data.schema";
import { createAuthDataDto } from "../dto/create-auth-data.dto";

@Injectable()
export class AuthDataService {
    constructor(
        @InjectModel('AuthData') private readonly authDataModel: Model<AuthData>
    ) {}

    async createAuthData(createAuthDataDto: createAuthDataDto) {
        const exist = await this.authDataModel.findOne({apiKey: createAuthDataDto.apiKey});
        if(exist) {
            throw new ConflictException('ApiKey already exists');
        }
        const authData = new this.authDataModel(createAuthDataDto);
        return await authData.save();
    }

    async getNewAuthData(previusId?: mongoose.Types.ObjectId | null) {
        if(previusId) {
            await this.authDataModel.findOneAndUpdate({ _id: previusId }, {
                expired: true
            }).exec();
        }

        const nextAuthData = await this.authDataModel.findOne({ expired: false }).exec();
        return nextAuthData;
    }

    async setExpired(id: mongoose.Types.ObjectId) {
        await this.authDataModel.findOneAndUpdate({ _id: id }, {
            expired: true
        }).exec();
    }

    async getAuthData() {
        return await this.authDataModel.find().exec();
    }
}