import { Injectable, NotFoundException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import axios from "axios";
import { generateXSignature } from "src/utils/encryption";
import { HotelDto, HotelsDto } from "../interfaces/content.interface";
import { InjectModel } from "@nestjs/mongoose";
import mongoose, { Model } from "mongoose";
import { AuthData } from "../schema/auth-data.schema";
import { InjectQueue } from "@nestjs/bull";
import { Queue } from "bull";
import { QueueDto } from "../dto/queue.dto";
import { AuthDataService } from "./auth-data.service";

@Injectable()
export class DataCollectorService {
    constructor(
        private readonly configService: ConfigService,
        private readonly authDataService: AuthDataService,
        @InjectModel('Hotel') private readonly hotelsModel: Model<HotelDto>,
        @InjectQueue('data-collector') private readonly dataCollectorQueue:Queue<QueueDto>
    ) {
        
    }

    CHUNK_SIZE = this.configService.get<number>('CHUNK_SIZE');

    async getHotels(from: number, to: number,apiKey: string, secretKey: string) {
        const uri = this.configService.get<string>('HOTELBEDS_URI');
        try {
            let total = 0;
            const xSignature = generateXSignature(secretKey, apiKey);
            const request = await axios.get<HotelsDto>(`${uri}/hotel-content-api/1.0/hotels`, {
                headers: {
                    'X-Signature': xSignature.xSignature,
                    'Api-key': apiKey,
                    'Accept': 'application/json'
                },
                params: {
                    'fields': 'all',
                    'from': from,
                    'to': to,
                    'useSecondaryLanguage': false,
                    'language': 'ENG'
                },
                timeout: 60000,
                // onDownloadProgress: (progressEvent) => {
                //     console.log(progressEvent);
                // },
                maxContentLength: 1000000000,

            });
            for(const hotel of request.data.hotels){
                const newHotel = await this.hotelsModel.findOneAndUpdate({code: hotel.code}, hotel, {upsert: true, new: true}).exec();
                total++;
            }
            console.group('HotelBeds data collected');
            console.log({total: request.data.total,saved: total,from: from, to: to});
            console.groupEnd();
            return {total: request.data.total,saved: total,from: from, to: to,status: 'success'};

        } catch (err) {
           return {total: 0,saved: 0,from: from, to: to,status: 'error'}
        }
    }

    async getStatus(){
        const apiKey = this.configService.get<string>('HOTELBEDS_API_KEY');
        const secretKey = this.configService.get<string>('HOTELBEDS_SECRET_KEY');
        const uri = this.configService.get<string>('HOTELBEDS_URI');
        try {
            const xSignature = generateXSignature(secretKey, apiKey);
            const request = await axios.get<HotelsDto>(`${uri}/hotel-api/1.0/status`, {
                headers: {
                    'X-Signature': xSignature.xSignature,
                    'Api-key': apiKey,
                    'Accept': 'application/json'
                },
                timeout: 60000,
                onDownloadProgress: (progressEvent) => {
                    console.log(progressEvent);
                },
                maxContentLength: 1000000000,

            });
            return request.data;

        } catch (err) {
            throw err;
        }

    }



    async createDataCollectorChunks(){
        let authData = await this.authDataService.getNewAuthData();
        if(!authData){
            throw new NotFoundException('No auth data');
        }
        const apiKey = authData.apiKey;
        const secretKey = authData.secretKey;
        let result = await this.getHotels(0, this.CHUNK_SIZE-1, apiKey, secretKey);
        if(result.status === 'error'){
            await this.authDataService.setExpired(authData._id);
            throw new NotFoundException('No auth data');
        }
        let total = result.total;
        // temp change to 100
        //total = 100;
        const chunks = Math.ceil(total / this.CHUNK_SIZE);
        for(let i = 1; i < chunks; i++){
            await this.dataCollectorQueue.add({from: i * this.CHUNK_SIZE, to: (i + 1) * this.CHUNK_SIZE-1});
        }
        return {total: total, chunks: chunks};
    }

    async getTotalHotels(){
        return await this.hotelsModel.estimatedDocumentCount().exec();
    }

}