import { ApiProperty } from "@nestjs/swagger";

export class createAuthDataDto {
    @ApiProperty()
    apiKey: string;
    @ApiProperty()
    secretKey: string;
}